<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tentukan-nilai kristian</title>
</head>
<body>
    <h3>1. Tentukan Nilai</h3>
    <?php
        function tentukan_nilai($number)
        {
            //  kode disini
            if ($number >= 85 && $number = 100){
                return "Sangat Baik <br>";
            }
            else if ($number >= 70 && $number < 85){
                return "Baik <br>";
            }
            else if ($number >= 60 && $number <70){
                return "Cukup <br>";
            }
            else if ($number < 60){
                return "Kurang <br>";
            }
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>