<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tukar-besar-kecil kristian</title>
</head>
<body>
    <h3>3. Tukar Besar Kecil</h3>
    <?php
        function tukar_besar_kecil($string){
            //kode di sini
            $lower = strtolower($string);
            $upper = strtoupper($string);
            if ($string = $lower) {
                return "$upper <br>";
            }
            else if ($string = $upper) {
                return "$lower <br>";
            }
        }

        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
?>
    
</body>
</html>